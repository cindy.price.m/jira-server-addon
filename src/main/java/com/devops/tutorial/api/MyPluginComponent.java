package com.devops.tutorial.api;

public interface MyPluginComponent {
    String getName();
}